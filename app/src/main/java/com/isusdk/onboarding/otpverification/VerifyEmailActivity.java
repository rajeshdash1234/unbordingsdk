package com.isusdk.onboarding.otpverification;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;



public class VerifyEmailActivity extends AppCompatActivity {
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_email);
        Config.whiteStatusNav(this);

        submit = findViewById(R.id.verify_email_proceed);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(VerifyEmailActivity.this, WelcomeActivity.class);
                intent.putExtra("USER_TYPE", "new");
                startActivity(intent);
            }
        });
    }
}