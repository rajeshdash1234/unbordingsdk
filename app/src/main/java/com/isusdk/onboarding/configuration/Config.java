package com.isusdk.onboarding.configuration;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isusdk.onboarding.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Config {
    static final String otpURL = "https://us-central1-iserveustaging.cloudfunctions.net/OTPfunctions/api/v1/OTP";
    static final String pincodeURL = "https://us-central1-iserveustaging.cloudfunctions.net/pincodeFetch/api/v1/getCityStateAndroid";
    static final String rblURL = "https://csp.iserveu.tech/CSP/user/initcsp";
    static final String userIdURL = "https://us-central1-creditapp-29bf2.cloudfunctions.net/disbursement/videokyc/generatelinkforvdokyc";
    static final String activateUserURL = "https://us-central1-iserveustaging.cloudfunctions.net/onboardingstatus/api/v1/changeKYCverified";
    static final String requeryURL = "https://csp.iserveu.tech/CSP/CSP/requery";
    static final String panURL = "https://csp.iserveu.tech/apitestprod/validatePanCard";
    static final String loginURL = "https://wallet.iserveu.online/COREUSER/getlogintoken";
    //static final String elasticUser = "https://elasticuserprod-vn3k2k7q7q-uc.a.run.app/check/user_name";
    static final String elasticUser = "https://elasticuserprod.iserveu.website/check/user_name";
    static final String newUser = "https://wallet.iserveu.online/LIVE/api/gaweyeniuser";
//    static final String fetchUrl = "https://user-self-onboard-vn3k2k7q7q-uc.a.run.app/kycfetch";
    static final String fetchUrl = "https://userselfonboard.iserveu.website/kycfetch";
    //static final String saveUrl = "https://user-self-onboard-vn3k2k7q7q-uc.a.run.app/kycsave";
    static final String saveUrl = "https://userselfonboard.iserveu.website/kycsave";
    static final String digiLockerUrl = "https://us-central1-iserveustaging.cloudfunctions.net/digilocker/digiverify/";

    /*
    static final String fetchUrl = "https://us-central1-iserveustaging.cloudfunctions.net/user_self_onboard/kycfetch";
    static final String saveUrl = "https://us-central1-iserveustaging.cloudfunctions.net/user_self_onboard/kycsave";
    static final String rblURL = "https://newapp.iserveu.online/COREUSER/user/rbl/generateOtt";
    static final String newUser = "https://wallet.iserveu.online/BQ_TEST_IntW/api/gaweyeniuser";
    static final String requeryURL = "https://newapp.iserveu.online/COREUSER/user/rbl/requery"; // change
    static final String rblURL = "https://newapp.iserveu.online/CSP/user/initcsp"; // change
    static final String panURL = "https://newapp.iserveu.online/apitestprod/validatePanCard";
    static final String loginURL = "https://wallet.iserveu.online/TOKEN_Enable/getlogintoken.json"; // change
     */


    public static final String PHONE_KEY = "phoneKey";
    public static final String OTP_KEY = "otpKey";
    public static final String AADHAAR_KEY = "aadhaarKey";
    public static final String USER_ID_KEY = "userIdKey";
    public static final String USER_NAME_KEY = "userNameKey";
    public static final String USER_PASSWORD_KEY = "userPasswordKey";
    public static final String NAME_KEY = "nameKey";
    public static final String SHOP_KEY = "shopKey";
    public static final String DATE_KEY = "dateKey";
    public static final String ADDRESS_KEY = "addressKey";
    public static final String STATE_KEY = "stateKey";
    public static final String CITY_KEY = "cityKey";
    public static final String PIN_KEY = "pinKey";
    public static final String MAIL_KEY = "mailKey";
    public static final String PAN_CARD_KEY = "panKey";
    public static final String DOCUMENT_KEY = "docKey";
    public static final String CLIENT_REF_KEY = "clientRefKey";
    public static final String PAN_KEY = "panKey";
    public static final String FIRST_NAME_KEY = "firstNameKey";
    public static final String LAST_NAME_KEY = "lastNameKey";

    public static boolean isNewUser = true;
    public static String userAdmin ="demoisu";
    public static String createBy = "demoisu";

    public static Map<String, Object> document = new HashMap<>();

    public static JSONObject docObject = new JSONObject();

    public static Map<String, Object> getDocument() {
        return document;
    }

    public static JSONObject getDocObject() {
        return docObject;
    }

    public static void setDocObject(JSONObject docObject) {
        Config.docObject = docObject;
    }

    public static void setDocument(Map<String, Object> document) {
        Config.document = document;
    }

    public static String getPanURL() {
        return panURL;
    }

    public static String getActivateUserURL() {
        return activateUserURL;
    }

    public static String getUserIdURL() {
        return userIdURL;
    }

    public static String getRblURL() {
        return rblURL;
    }

    public static String getOtpURL() {
        return otpURL;
    }

    public static String getPincodeURL() {
        return pincodeURL;
    }

    public static String getRequeryURL() {
        return requeryURL;
    }

    public static String getLoginURL() {
        return loginURL;
    }

    public static String getElasticUser() {
        return elasticUser;
    }

    public static String getDigiLockerUrl() {
        return digiLockerUrl;
    }

    public static String getNewUser() {
        return newUser;
    }

    public static boolean isIsNewUser() {
        return isNewUser;
    }

    public static void setIsNewUser(boolean isNewUser) {
        Config.isNewUser = isNewUser;
    }

    public static String getFetchUrl() {
        return fetchUrl;
    }

    public static String getSaveUrl() {
        return saveUrl;
    }

    public static Map<String, Object> getFirestoreData(Context context, FirebaseFirestore db, String doc) {
        document = null;
        DocumentReference docRef = db.collection("NewUserInfoCollection").document(doc);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    if (task.getResult().getData() != null) {
                        document = task.getResult().getData();
                    }
                } else {
                    Toast.makeText(context, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }

        });
        return document;
    }

    public static void whiteStatusNav(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                window.setNavigationBarColor(ContextCompat.getColor(activity, R.color.white));
                window.setStatusBarColor(ContextCompat.getColor(activity, R.color.white));
            } else {
                window.setNavigationBarColor(ContextCompat.getColor(activity, R.color.darkText));
                window.setStatusBarColor(ContextCompat.getColor(activity, R.color.darkText));
            }
        }
    }
}

/*

     1 - basic info
     2 - ekyc complete
     3 - requery api
     4 - pan info
     5 - matching


AREA                    "area": "3rd Lane, Chintamaniswar /  / //AREA",
AUTHENTICATION_CODE     "authenticationcode": "8ec157ca585d450480c93c332c4ab086",
BUILDING                "building": "PLOT NO-2021",
CARE_OF                 "careOf": null,
CITY                    "city": "Bhubaneswar",
COUNTRY                 "country": "India",
DISTRICT                "district": "Khordha",
DOB                     "dob": "02-06-1996",
GENDER                  "gender": "M",
NAME                    "name": "Manas Ranjan Sahoo",
PHOTO                   "photo": null,
PIN                     "pin": "751006",
RRN                     "rrn": "025217816834",
STATE                   "state": null,
STREET                  "street": "",
TXN_ID                  "txnid": "UKC:816834"
UIDTOKEN                "uidtoken": "01000741XxcPCDalTHVN46qhMN6W9KkPQxtv/iGm/l49TDN1nu7hCaVEqkfgAeCLnq8m/X3Z",
UNIQUE_REQUEST_ID       "uniquerequestid": "ISUBCSP762655324815376",
USER_ID                 "Yx36NZTp87wsryX7GAsu"
USER_NAME               "pr@123456"


LAST_UPDATE_DATE    "last-update-date": "12/04/2018",
PAN_NAME            "filler1": "MANAS RANJAN SAHOO"
PAN_NUMBER          "pan": "JZVPS7850D"
PAN_STATUS          "panstatus": "E"
PAN_TITLE           "pan-title": "Shri"
PAN_URL             imageURL
USER_ID             uid
USER_NAME           "itpl"

maxm size 5mb
file format jpeg, png

 */