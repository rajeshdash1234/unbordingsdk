package com.isusdk.onboarding.otpverification;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isusdk.onboarding.R;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OnboardingLoginActivity extends AppCompatActivity {
    private static final String TAG = OnboardingLoginActivity.class.getSimpleName();
    Button login;
    EditText nameET, passwordET;
    String name, password;
    ProgressDialog dialog;
    OnBoardSharedPreference preference;
    boolean isPasswordVisible = false;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_onboarding_login);
        Config.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);
        preference.clearBoardingPref();
//        nameET = findViewById(R.id.user_login_user);
//        passwordET = findViewById(R.id.user_login_password);
//        login = findViewById(R.id.user_login);

        JSONObject obj = new JSONObject();
        JSONArray array = new JSONArray();
        array.put("number");
        try {
            obj.put("pancards", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "verifyPan: obj "+obj );

//        nameET.setText("aepsTestR");
//        passwordET.setText("aepsTestR");

        passwordET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (passwordET.getRight() - passwordET.getCompoundDrawables()[RIGHT].getBounds().width())) {
                        int selection = passwordET.getSelectionEnd();
                        if (isPasswordVisible) {
                            // set drawable image
//                            passwordET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility, 0);
                            // hide Password
                            passwordET.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            isPasswordVisible = false;
                        } else {
                            // set drawable image
                            // set drawable image
//                            passwordET.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_visibility_off, 0);
                            // show Password
                            passwordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            isPasswordVisible = true;
                        }
                        passwordET.setSelection(selection);
                        return true;
                    }
                }
                return false;
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameET.getText().toString();
                password = passwordET.getText().toString();
                if (name.equals("")) {
                    Toast.makeText(OnboardingLoginActivity.this, "Enter User ID", Toast.LENGTH_LONG).show();
                } else if (password.equals("")) {
                    Toast.makeText(OnboardingLoginActivity.this, "Enter Password", Toast.LENGTH_LONG).show();
                } else {
                    makeLogin(name, password);
                }
            }
        });

    }

    private void makeLogin(String id, String pw) {
        dialog = new ProgressDialog(OnboardingLoginActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();
        JSONObject obj = new JSONObject();

        preference.setStringValue(Config.USER_NAME_KEY, id);

        try {
            obj.put("username", id);
            obj.put("password", pw);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Config.getLoginURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Toast.makeText(OnboardingLoginActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                        try {
                            String token = response.getString("token");
                            showSessionAlert("Your token:\n" + token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: getErrorBody " + anError.getErrorBody());
                        Log.e(TAG, "onError: getErrorDetail " + anError.getErrorDetail());
                        Log.e(TAG, "onError: getResponse " + anError.getResponse());
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            String status = errorObject.getString("status");
                            if (status.equals("-1")) {
                                showNotActivated(errorObject.getString("statusDesc"));
//                                Toast.makeText(OnboardingLoginActivity.this, errorObject.getString("statusDesc"), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            } else {
                                Toast.makeText(OnboardingLoginActivity.this, errorObject.getString("message"), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(OnboardingLoginActivity.this, anError.getErrorDetail(), Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                    }
                });
    }

    public void showNotActivated(String statusDesc) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(statusDesc)
                    .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
//                            startActivity(new Intent(OnboardingLoginActivity.this, UserOtpActivity.class));
                            dialog.dismiss();
                            //finish();
                        }
                    });

//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }
    }

    public void showSessionAlert(String message) {
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
            // String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Success")
                    .setMessage(message)
                    .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            //finish();
                        }
                    });

//                    .show();
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        } catch (Exception e) {

        }


    }
}