package com.isusdk.onboarding.ekyc;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isusdk.onboarding.R;
import com.isusdk.onboarding.Util;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;
import com.isusdk.onboarding.configuration.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EkycFormActivity extends AppCompatActivity {
    private static final String TAG = EkycFormActivity.class.getSimpleName();

    EditText nameET, addressET, stateET, cityET, pinET, phoneET, mailET, panET, shopET, aadhaarET;
    TextView dobET;
    String name, dob, address, state, city, pin, phone, mail, pan, shop, aadhaar;

    private int mYear, mMonth, mDay;

    ProgressDialog dialog;
    OnBoardSharedPreference preference;

    Map<String, Object> mapData;

    boolean freshUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekyc_form);
        Config.whiteStatusNav(this);
        preference = new OnBoardSharedPreference(this);
        phone = preference.getStringValue(Config.PHONE_KEY);
        freshUser = true;

        shopET = findViewById(R.id.ekyc_form_shop);
        aadhaarET = findViewById(R.id.ekyc_form_aadhaar);
        nameET = findViewById(R.id.ekyc_form_name);
        dobET = findViewById(R.id.ekyc_form_date);
        addressET = findViewById(R.id.ekyc_form_address);
        stateET = findViewById(R.id.ekyc_form_state);
        cityET = findViewById(R.id.ekyc_form_city);
        pinET = findViewById(R.id.ekyc_form_pin);
        phoneET = findViewById(R.id.ekyc_form_phone);
        mailET = findViewById(R.id.ekyc_form_mail);
        panET = findViewById(R.id.ekyc_form_pan);
        dob = "";

        if (Config.isIsNewUser()) {
            shopET.setVisibility(View.VISIBLE);
            aadhaarET.setVisibility(View.VISIBLE);
        }

        phoneET.setText(phone);

        panET.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                    s = s.toUpperCase();
                    panET.setText(s);
                    panET.setSelection(panET.length()); //fix reverse texting
                }
            }
        });

        aadhaarET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (Config.isIsNewUser()) {
                    aadhaar = aadhaarET.getText().toString();
                    if (!Util.validateAadharNumber(aadhaar)) {
                        aadhaarET.setError(getResources().getString(R.string.valid_aadhar_error));
                    } else {
                        aadhaarET.setError(null);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        dobET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(EkycFormActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                monthOfYear = monthOfYear + 1;
                                String month = monthOfYear + "";
                                if (monthOfYear < 10) {
                                    month = "0" + monthOfYear;
                                }
                                String day = dayOfMonth + "";
                                if (dayOfMonth < 10) {
                                    day = "0" + dayOfMonth;
                                }

                                dob = day + "-" + month + "-" + year;


//                                dobET.setText(day + "-" + month + "-" + year);
                                dobET.setText(formatDate(dob));
                            }
                        }, mYear, mMonth, mDay);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.YEAR, -18);
                datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        pinET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pin = String.valueOf(charSequence);

                if (pin.length() == 6) {
//                    if (!getIntent().getStringExtra("data").equals("1")) {
                    if (freshUser) {
                        int pincode = Integer.parseInt(pin);
                        Log.e(TAG, "onTextChanged: pincode " + pincode);
                        parsePin(pincode);
                    } else {
                        freshUser = true;
                    }
                } else {
                    cityET.setText("");
                    stateET.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        if (getIntent().getStringExtra("data").equals("1")) {
            freshUser = false;
            //make the autofill make registration
//            mapData = Config.getDocument();
//            Map<String, Object> basicInfoMap = (Map<String, Object>) mapData.get("basicInfo");

            JSONObject docObject = Config.getDocObject();
            Log.e(TAG, "onCreate: doc object " + docObject);

            try {
                JSONObject basicObject = docObject.getJSONObject("basicInfo");
                Log.e(TAG, "onCreate: basicObject " + basicObject);
                String mapName = basicObject.getString("NAME");
                String mapDob = basicObject.getString("DOB");
                String mapAddress = basicObject.getString("ADDRESS");
                String mapPin = basicObject.getString("PINCODE");
                String mapState = basicObject.getString("STATE");
                String mapCity = basicObject.getString("CITY");
                String mapMail = basicObject.getString("EMAIL");
                String mapShop = "";
                String mapAadhaar = "";
                if (basicObject.has("SHOP")){
                    mapShop = basicObject.getString("SHOP");
                    shopET.setText(mapShop);
                }
                if (basicObject.has("AADHAAR")){
                    mapAadhaar = basicObject.getString("AADHAAR");
                    aadhaarET.setText(mapAadhaar);
                }
//                String mapPan = basicObject.getString("PAN_NO");

//                Log.e(TAG, "onCreate: mapName " + mapName);
//                Log.e(TAG, "onCreate: mapDob " + newFormatDate(mapDob));
//                Log.e(TAG, "onCreate: mapAddress " + mapAddress);
//                Log.e(TAG, "onCreate: mapPin " + mapPin);
//                Log.e(TAG, "onCreate: mapState " + mapState);
//                Log.e(TAG, "onCreate: mapCity " + mapCity);
//                Log.e(TAG, "onCreate: mapMail " + mapMail);
//                Log.e(TAG, "onCreate: mapPan " + mapPan);

                nameET.setText(mapName);
                dob = mapDob;
                if (dob.contains("-")) {
                    dobET.setText(formatDate(mapDob));
                } else {
                    dobET.setText(newFormatDate(mapDob));
                }
                addressET.setText(mapAddress);
                pinET.setText(mapPin);
                stateET.setText(mapState);
                cityET.setText(mapCity);
                mailET.setText(mapMail);

                city = mapCity;
                state = mapState;
//                panET.setText(mapPan);
            } catch (JSONException e) {
                e.printStackTrace();
            }


//            generateURL(mapName, mapDob, mapAddress, mapPin, mapState, mapCity, phone, mapMail, mapPan);
        }

        findViewById(R.id.ekyc_form_prev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.ekyc_form_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = nameET.getText().toString();
                address = addressET.getText().toString();
                pin = pinET.getText().toString();
                mail = mailET.getText().toString();
                pan = panET.getText().toString();
                phone = phoneET.getText().toString();
                shop = shopET.getText().toString();
                aadhaar = aadhaarET.getText().toString();

                if (!Validate.isValidName(name)) {
                    Validate.showAlert(EkycFormActivity.this, "Enter a valid name");
                } else if (dob.equals("")) {
                    Validate.showAlert(EkycFormActivity.this, "Select your date of birth");
                } else if (address.equals("")) {
                    Validate.showAlert(EkycFormActivity.this, "Enter your address");
                } else if (!Validate.isAddress(address)) {
                    Validate.showAlert(EkycFormActivity.this, "Enter a valid address");
                } else if (!Validate.isValidPhone(phone)) {
                    Validate.showAlert(EkycFormActivity.this, "Enter a valid mobile number");
                } else if (pin.length() != 6) {
                    Validate.showAlert(EkycFormActivity.this, "Enter PIN Code");
                } else if (city.equals("")){
                    Validate.showAlert(EkycFormActivity.this, "Enter a valid PIN code of your city");
                } else if (state.equals("")){
                    Validate.showAlert(EkycFormActivity.this, "Enter a valid PIN code of your state");
                }
                else if (!Validate.isValidMail(mail)) {
                    Validate.showAlert(EkycFormActivity.this, "Enter a valild e-mail address");
//                } else if (pan.equals("")) {
//                    Validate.showAlert(EkycFormActivity.this, "Please enter your PAN Card Number");
                } else if (Config.isIsNewUser()) {
                    String surName = name.split(" ")[name.split(" ").length - 1];
                    String firstName = name.substring(0, name.length() - surName.length());
                    if (surName.equals("")){
                        surName = "N/A";
                    }

                    preference.setStringValue(Config.FIRST_NAME_KEY, firstName);
                    preference.setStringValue(Config.LAST_NAME_KEY, surName);

                    if (shop.trim().equals("")) {
                        Validate.showAlert(EkycFormActivity.this, "Enter a valid shop name");
                    } else if (!Util.validateAadharNumber(aadhaar)) {
                        Validate.showAlert(EkycFormActivity.this, "Enter a valid aadhaar number");
                    } else {
                        preference.setStringValue(Config.SHOP_KEY, shop);
                        preference.setStringValue(Config.AADHAAR_KEY, aadhaar);
                        preference.setStringValue(Config.MAIL_KEY, mail);
                        preference.setStringValue(Config.CITY_KEY, city);
                        preference.setStringValue(Config.STATE_KEY, state);
                        preference.setStringValue(Config.ADDRESS_KEY, address);
                        preference.setStringValue(Config.PIN_KEY, pin);
                        state = stateET.getText().toString();
                        city = cityET.getText().toString();
                        storeBasicInfo(name, dob, address, pin, state, city, phone, mail, aadhaar, shop);
                    }
                } else {
                    state = stateET.getText().toString();
                    city = cityET.getText().toString();
                    preference.setStringValue(Config.MAIL_KEY, mail);
                    preference.setStringValue(Config.CITY_KEY, city);
                    preference.setStringValue(Config.STATE_KEY, state);
                    preference.setStringValue(Config.ADDRESS_KEY, address);
                    preference.setStringValue(Config.PIN_KEY, pin);
                    storeBasicInfo(name, dob, address, pin, state, city, phone, mail, "", "");
                }

            }
        });
    }

    private void storeBasicInfo(String genName, String gendob, String genAddress, String genPin, String genState, String genCity, String genPhone, String genMail, String genAadhaar, String genShop) {
        dialog = new ProgressDialog(EkycFormActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            JSONObject dataObject = new JSONObject();
            dataObject.put("redirUrl", "android");
            dataObject.put("username", preference.getStringValue(Config.USER_NAME_KEY));
            dataObject.put("name", genName);
            dataObject.put("dob", gendob);
            dataObject.put("address", genAddress);
            dataObject.put("city", genCity);
            dataObject.put("state", genState);
            dataObject.put("pin", genPin);
            dataObject.put("email", genMail);
            dataObject.put("phone", genPhone);
            dataObject.put("shop", genShop);
            dataObject.put("aadhaar", genAadhaar);

            obj.put("subStatus", "1");
            obj.put("type", "BASIC_INFO");
            obj.put("username", preference.getStringValue(Config.USER_NAME_KEY));
            obj.put("data", dataObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "storeBasicInfo: obj "+obj );

        AndroidNetworking.post(Config.getSaveUrl())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "onResponse: " + response);
                        try {
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                JSONObject data = response.getJSONObject("data");
                                String clientRefNo = response.getString("clientRefNo");
                                preference.setStringValue(Config.CLIENT_REF_KEY, clientRefNo);
                                String cpuniquerefno = data.getString("cpuniquerefno");
                                String url = data.getString("url");
                                Intent intent = new Intent(EkycFormActivity.this, RblLinkActivity.class);
                               // Intent intent = new Intent(EkycFormActivity.this, PanVerificationActivity.class);
                                intent.putExtra("url", url + "&cpuniquerefno=" + cpuniquerefno);
                                dialog.dismiss();
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(EkycFormActivity.this, "Please try again", Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d(TAG, "onResponse: "+e);
                            Toast.makeText(EkycFormActivity.this, "Please try again", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(TAG, "onError: body " + anError.getErrorBody());
                        Log.e(TAG, "onError: details " + anError.getErrorDetail());
                        try {
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Toast.makeText(EkycFormActivity.this, errorObject.getString("statusDesc"), Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(EkycFormActivity.this, "Please try again later!", Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                    }
                });

    }

    private void generateURL(String genName, String gendob, String genAddress, String genPin, String genState, String genCity, String genPhone, String genMail, String getPan) {
        dialog = new ProgressDialog(EkycFormActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        String clientReFno = getClientRefNo(preference.getStringValue(Config.USER_ID_KEY));

        Log.e(TAG, "generateURL: " + clientReFno);
        try {
//            obj.put("redirUrl", "android");
//            obj.put("callBackUrl", "android");
//            obj.put("clientRefNo", clientReFno);
//            obj.put("pincode", genPin);
//            obj.put("address", genAddress);
//            obj.put("city", genCity);
//            obj.put("mobile", genPhone);
//            obj.put("dob", gendob);
//            obj.put("name", genName);
//            obj.put("state", genState);
//            obj.put("pan", "");
//            obj.put("email", genMail);

            obj.put("username", preference.getStringValue(Config.USER_NAME_KEY));
            obj.put("name", genName);
            obj.put("dob", gendob);
            obj.put("address", genAddress);
            obj.put("state", genState);
            obj.put("city", genCity);
            obj.put("pincode", genPin);
            obj.put("mobile", genPhone);
            obj.put("email", genMail);
            obj.put("pan", "");
            obj.put("redirUrl", "android");


//            "username":$rootScope.UserName,
//                    "name": $scope.userInfo.fullName,
//                    "dob": dob,
//                    "address": $scope.userInfo.address,
//                    "state": $scope.userInfo.state,
//                    "city": $scope.userInfo.city,
//                    "pincode": $scope.userInfo.pincode,
//                    "mobile": $scope.userInfo.mobileNumber,
//                    "email": $scope.userInfo.email,
//                    "pan": "",
//                    "redirUrl": "http://localhost:3000/#/CSPFormNew"

        } catch (JSONException e) {
            e.printStackTrace();
        }

        preference.setStringValue(Config.NAME_KEY, genName);

        Log.e(TAG, "generateURL: json " + obj);

        AndroidNetworking.post(Config.getRblURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "onResponse: " + response);
                            String status = response.getString("status");
                            if (status.equals("0")) {
                                String url = response.getString("url");
                                String cpuniquerefno = response.getString("cpuniquerefno");

                                //put data in firestore for sub_status 1

                                FirebaseFirestore db = FirebaseFirestore.getInstance();

                                Map<String, Object> document = new HashMap<>();
                                Map<String, Object> basicInfo = new HashMap<>();

                                basicInfo.put("ADDRESS", genAddress);
                                basicInfo.put("CITY", genCity);
                                basicInfo.put("DOB", gendob);
                                basicInfo.put("EMAIL", genMail);
                                basicInfo.put("NAME", genName);
//                                basicInfo.put("PAN_NO", getPan);
                                basicInfo.put("PHONE", genPhone);
                                basicInfo.put("PINCODE", genPin);
                                basicInfo.put("STATE", genState);
                                basicInfo.put("USER_ID", preference.getStringValue(Config.USER_ID_KEY));
                                basicInfo.put("USER_NAME", preference.getStringValue(Config.USER_NAME_KEY));
//                                basicInfo.put("C_REF_NO", clientReFno);

                                preference.setStringValue(Config.CLIENT_REF_KEY, clientReFno);

                                document.put("SUB_STATUS", "1");
                                document.put("basicInfo", basicInfo);

                                db.collection("NewUserInfoCollection").document(preference.getStringValue(Config.USER_NAME_KEY))
                                        .set(document)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.e(TAG, "DocumentSnapshot successfully written!");
                                                Config.setDocument(document);
                                                Intent intent = new Intent(EkycFormActivity.this, RblLinkActivity.class);
                                                intent.putExtra("url", url + "&cpuniquerefno=" + cpuniquerefno);
                                                dialog.dismiss();
                                                startActivity(intent);
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.e(TAG, "Error writing document", e);
                                                dialog.dismiss();
                                            }
                                        });
                            }
                        } catch (JSONException e) {
                            Log.e(TAG, "onResponse: " + e);
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            Log.e(TAG, "onError: " + anError.getErrorDetail());
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            Log.e(TAG, "onError: " + errorObject);
                            String statusDesc = errorObject.getString("statusdesc");
                            Toast.makeText(EkycFormActivity.this, statusDesc, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                });


    }

    private String getClientRefNo(String value) {
        StringBuilder v = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            v.append((int) value.charAt(i));
        }
        v.append(System.currentTimeMillis());
        return v.toString();
    }

    private void parsePin(int pin) {
        dialog = new ProgressDialog(EkycFormActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();

        try {
            obj.put("pin", pin);
//            obj.put("type", "production");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Config.getPincodeURL())
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int statusCode = response.getInt("statusCode");
                            if (statusCode == 0) {
                                JSONObject data = response.getJSONObject("data");
                                String status = data.getString("status");
                                if (status.equalsIgnoreCase("success")) {
                                    state = data.getJSONObject("data").getString("state");
                                    city = data.getJSONObject("data").getString("city");

                                    stateET.setText(getStateName(state));
                                    cityET.setText(city);
                                } else {
                                    state = "";
                                    city = "";
                                    Toast.makeText(EkycFormActivity.this, "Try Again", Toast.LENGTH_LONG).show();
                                }
                            }

                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {

                            // 0 mismatch
                            // -1 expired
                            // 1 success
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            JSONObject data = errorObject.getJSONObject("data");
                            String statusDesc = data.getString("statusDesc");
                            Toast.makeText(EkycFormActivity.this, statusDesc, Toast.LENGTH_LONG).show();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        state = "";
                        city = "";
                    }
                });
    }

    private String formatDate(String dt) {
        String pattern = "dd-MM-yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date d = null;
        if (dt != null) {
            try {
                d = simpleDateFormat.parse(dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("E, dd MMM yyyy");
        return formatter.format(d);
    }

    private String newFormatDate(String dt) {
        String pattern = "dd/MM/yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Date d = null;
        if (dt != null) {
            try {
                d = simpleDateFormat.parse(dt);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
//        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("E, dd MMM yyyy");
        return formatter.format(d);
    }

    public String getStateName(String s) {
        String name = s;
        List<String> shortState = new ArrayList<String>();
        List<String> longState = new ArrayList<String>();


        shortState.add("AN");
        longState.add("Andaman & Nicobar");
        shortState.add("AP");
        longState.add("Andhra Pradesh");
        shortState.add("AR");
        longState.add("Arunachal Pradesh");
        shortState.add("AS");
        longState.add("Assam");
        shortState.add("BR");
        longState.add("Bihar");
        shortState.add("CH");
        longState.add("Chandigarh");
        shortState.add("CG");
        longState.add("Chhattisgarh");
        shortState.add("DN");
        longState.add("Dadra and Nagar Haveli");
        shortState.add("DD");
        longState.add("Daman & Diu");
        shortState.add("DL");
        longState.add("Delhi");
        shortState.add("GA");
        longState.add("Goa");
        shortState.add("GJ");
        longState.add("Gujarat");
        shortState.add("HR");
        longState.add("Haryana");
        shortState.add("HP");
        longState.add("Himachal Pradesh");
        shortState.add("JK");
        longState.add("Jammu & Kashmir");
        shortState.add("JH");
        longState.add("Jharkhand");
        shortState.add("KA");
        longState.add("Karnataka");
        shortState.add("KL");
        longState.add("Kerala");
        shortState.add("LD");
        longState.add("Lakshadweep");
        shortState.add("MP");
        longState.add("Madhya Pradesh");
        shortState.add("MH");
        longState.add("Maharashtra");
        shortState.add("MN");
        longState.add("Manipur");
        shortState.add("ML");
        longState.add("Meghalaya");
        shortState.add("MZ");
        longState.add("Mizoram");
        shortState.add("NL");
        longState.add("Nagaland");
        shortState.add("OR");
        longState.add("Odisha");
        shortState.add("PY");
        longState.add("Puducherry");
        shortState.add("PB");
        longState.add("Punjab");
        shortState.add("RJ");
        longState.add("Rajasthan");
        shortState.add("SK");
        longState.add("Sikkim");
        shortState.add("TN");
        longState.add("Tamil Nadu");
        shortState.add("TG");
        longState.add("Telangana");
        shortState.add("TR");
        longState.add("Tripura");
        shortState.add("UP");
        longState.add("Uttar Pradesh");
        shortState.add("UK");
        longState.add("Uttarakhand");
        shortState.add("WB");
        longState.add("West Bengal");

        if (shortState.contains(name)) {
            name = longState.get(shortState.indexOf(s));
        }

        return name;
    }

}