package com.isusdk.onboarding.forgotpassword;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.isusdk.onboarding.R;
import com.isusdk.onboarding.login.LoginActivity;
import com.isusdk.onboarding.utility.ConnectionDetector;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class ForgotPasswordActivity extends AppCompatActivity {

    TextInputEditText user_name, otp;
    TextInputLayout otpLayout, nameLayout;
    Button sendOtp, verifyOtp;
    String userName;
    int userOtp = 0;
    ProgressBar progressBar;
    TextView resendotp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        user_name = findViewById(R.id.user_name);
        otp = findViewById(R.id.otp);
        sendOtp = findViewById(R.id.sendOtp);
        otpLayout = findViewById(R.id.otpLayout);
        nameLayout = findViewById(R.id.nameLayout);
        verifyOtp = findViewById(R.id.verifyOtp);
        progressBar = findViewById(R.id.progressBar);
        resendotp = findViewById(R.id.resendotp);

        if (user_name.hasFocus()){
            user_name.setCursorVisible(true);
        }

        resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (user_name.equals("") || user_name.equals(null)) {

                    Toast.makeText(ForgotPasswordActivity.this, "Enter the registered User Name", Toast.LENGTH_SHORT).show();

                } else {
                    /*Call api to send the otp to the registered mobile number*/
                    ConnectionDetector cd = new ConnectionDetector(
                            ForgotPasswordActivity.this);
                    if (cd.isConnectingToInternet()) {
                        userName = user_name.getText().toString().trim();

                        if (userName.length() != 0) {
                            progressBar.setVisibility(View.VISIBLE);
                            getEncodeUrlCode();


                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, "Please check credential", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (user_name.length() == 0) {

                    Toast.makeText(ForgotPasswordActivity.this, "Enter the registered User Name", Toast.LENGTH_SHORT).show();

                } else {
                    /*Call api to send the otp to the registered mobile number*/
                    ConnectionDetector cd = new ConnectionDetector(
                            ForgotPasswordActivity.this);
                    if (cd.isConnectingToInternet()) {
                        userName = user_name.getText().toString().trim();

                        if (userName.length() != 0) {
                            progressBar.setVisibility(View.VISIBLE);
                            getEncodeUrlCode();


                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, "Please check credential", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        verifyOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (otp.length() == 0) {

                    Toast.makeText(ForgotPasswordActivity.this, "Enter the OTP ", Toast.LENGTH_SHORT).show();

                } else {
                    /*Call api to send the otp to the registered mobile number*/
                    ConnectionDetector cd = new ConnectionDetector(
                            ForgotPasswordActivity.this);
                    if (cd.isConnectingToInternet()) {
                        String otpvalue = otp.getText().toString().trim();
                        userOtp = Integer.parseInt(otpvalue);

                        if (userOtp != 0) {
                            progressBar.setVisibility(View.VISIBLE);
                            verifyEncodeUrlCode();


                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, "Please check credential", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

    private void verifyEncodeUrlCode() {

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v86")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            verifyOtp(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }

                });
    }

    private void verifyOtp(String encodedUrl) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", userName);
            obj.put("otp", userOtp);
            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                String msg = obj.getString("statusDesc");
                                if(status.equals("0")){
                                    Toast.makeText(ForgotPasswordActivity.this, msg, Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(i);
                                    finish();
                                }else{
                                    Toast.makeText(ForgotPasswordActivity.this, msg, Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.GONE);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(anError.getErrorBody().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getEncodeUrlCode() {

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v85")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            sendOtp(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }

                });
    }

    private void sendOtp(String base64) {
        //    base64 = "https://uatapps.iserveu.online/STAGEING/getlogintoken.json?";
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", userName);
            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                progressBar.setVisibility(View.GONE);
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                if (status.equals("0")) {
                                    verifyOtp.setVisibility(View.VISIBLE);
                                    otpLayout.setVisibility(View.VISIBLE);
                                    nameLayout.setVisibility(View.VISIBLE);
                                    resendotp.setVisibility(View.VISIBLE);
                                    sendOtp.setVisibility(View.GONE);
                                    Toast.makeText(ForgotPasswordActivity.this, obj.getString("statusDesc").toString(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(ForgotPasswordActivity.this, "", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.GONE);
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(anError.getErrorBody().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Toast.makeText(ForgotPasswordActivity.this, obj.getString("statusDesc").toString(), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}