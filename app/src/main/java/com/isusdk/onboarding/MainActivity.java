package com.isusdk.onboarding;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isusdk.onboarding.configuration.Config;
import com.isusdk.onboarding.configuration.OnBoardSharedPreference;
import com.isusdk.onboarding.login.LoginActivity;
import com.isusdk.onboarding.otpverification.NewUserActivity;
import com.isusdk.onboarding.otpverification.ResumeProcessActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {

    Button btn_onboarding;
    int selected = 0;
    OnBoardSharedPreference preference;
    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    public static final String USER_MPIN = "mpinKey";
    public static final String ADMIN_NAME = "adminNameKey";
    String firestoreAdmin;
    CardView cardView_createNewUser, cardView_resume_Process;
    ImageView imageView_tick_createUser, imageView_tick_resumeprocess;
    int selectProcess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        cardView_createNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView_tick_createUser.setVisibility(View.VISIBLE);
                imageView_tick_resumeprocess.setVisibility(View.GONE);
                selectProcess = 1;
            }
        });

        cardView_resume_Process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView_tick_createUser.setVisibility(View.GONE);
                imageView_tick_resumeprocess.setVisibility(View.VISIBLE);
                selectProcess = 2;
            }
        });

        btn_onboarding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preference.clearBoardingPref();
                Config.setIsNewUser(true);
                if (selectProcess == 2) {
                    startActivity(new Intent(MainActivity.this, ResumeProcessActivity.class));
                } else if (selectProcess == 1) {
                    startActivity(new Intent(MainActivity.this, NewUserActivity.class));
                }
            }
        });
    }

    private void initView() {
        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        preference = new OnBoardSharedPreference(this);
        firestoreAdmin = sp.getString(ADMIN_NAME, "");

        btn_onboarding = findViewById(R.id.btn_onBoarding);
        cardView_createNewUser = findViewById(R.id.cardView_createNewuser);
        cardView_resume_Process = findViewById(R.id.cardView_resumeProcess);
        imageView_tick_createUser = findViewById(R.id.tick_createnewUser);
        imageView_tick_resumeprocess = findViewById(R.id.tick_resumeProcess);
    }
}


